import express from 'express';
import controllers from '../controllers/posts';
const routers = express.Router();

routers.get('/posts', controllers.getPosts);

export default routers;

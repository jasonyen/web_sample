import { Request, Response, NextFunction } from 'express';

interface User {
    userId: String,
    name: String,
    email: String
}

const login = (req: Request, res: Response, next: NextFunction) => {
    const user: [User] = req.body;
    // if (user.userId === '1') {
    //     return res.status(200).json({
    //         result: 'OK'
    //     });
    // }
    return res.status(401).json({
        result: 'Unauthorized user'
    });
};

export default { login };
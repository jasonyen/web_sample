import { Request, Response, NextFunction } from 'express';
import axios, { AxiosResponse } from 'axios';

interface Post {
    userId: Number;
    id: Number;
    title: String;
    body: String;
}

const getPosts = async (req: Request, res: Response, next: NextFunction) => {
    let response: AxiosResponse = await axios.get('https://jsonplaceholder.typicode.com/posts');
    let posts: [Post] = response.data;
    return res.status(200).json({
        result: posts
    });
};

export default { getPosts };
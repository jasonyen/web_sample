import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import routes from './routes/posts';

const router: Express = express();

const whitelist = ['http://localhost:3000', 'http://developer2.com'];
const corsOptions = {
    origin: ['http://localhost:3000', 'http://localhost:8080', 'http://localhost', 'http://developer2.com'],  // Access-Control-Allow-Origin
    methods: ['GET', 'POST'],   // Access-Control-Allow-Methods
    allowedHeaders: ['Content-Type', 'Authorization'],  // Access-Control-Allow-Headers
    preflightContinue: false,
    optionsSuccessStatus: 200
};

/** Using cors policy */
router.use(cors(corsOptions));

/** Logging */
router.use(morgan('dev'));
/** Parse the request */
router.use(express.urlencoded({ extended: false }));
/** Takes care of JSON data */
router.use(express.json());

/** RULES OF OUR API */
router.use((req, res, next) => {
    // // set the CORS policy
    // res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    // // set the CORS headers
    // res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,Content-Type,Accept, Authorization');
    // // set the CORS method headers
    // if (req.method === 'OPTIONS') {
    //     res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
    //     return res.status(200).json({});
    // }
    next();
});

/** web entry */
// router.use('/', express.static(__dirname + '/dist'));
// router.get('/', (req, res) => {
//     res.sendFile(__dirname + '/dist/index.html');
// });

/** Routes */
router.use('/', routes);

/** Error handling */
router.use((req, res, next) => {
    const error = new Error('not found');
    return res.status(404).json({
        message: error.message
    });
});

/** Server */
const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 3000;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));

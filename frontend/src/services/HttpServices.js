import { useState } from 'react';
import Axios from 'axios';
import env from '@/utils/env';

// const headers = {
//     'Content-Type': 'application/json',
//     Accept: 'application/json',
// };

const HttpServices = () => {
    const defaultConfig = {
        baseURL: env.API_URL, // 以後會從 env 進來
        withCredentials: false,
    };
    const axios = Axios.create(defaultConfig);
    const [response, setResponse] = useState({});
    const exec = async (config = {}) => {
        let responseData = {};
        try {
            const { data } = await axios(config);
            responseData = data;
            setResponse(responseData);
        } catch (e) {
            onError(e);
        } finally {
            return responseData;
        }
    };

    return ({
        response,
        exec,
    });
};

export default HttpServices;
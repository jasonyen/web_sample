import React, { lazy, Suspense, useEffect } from "react";
import { render } from "react-dom";
import { HashRouter as Router, Route } from "react-router-dom";

const App = lazy(() => import("@/containers/App" /* webpackChunkName:"App" */));

const Main = () => (
    <Router>
        <Suspense fallback={<div>Module loading....</div>}>
            <Route path="/app" component={App} />
        </Suspense>
    </Router>
);

render(<Main />, document.getElementById("app"));
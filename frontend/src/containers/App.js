import React, { lazy, Suspense, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import HttpServices from '@/services/HttpServices.js';

const App = () => {
    const httpServices = HttpServices();
    const callAPI = async () => {
        const config = {
            url: `/posts`,
            method: "get"
          };
        const { result } = await httpServices.exec(config);
        // console.log('result', result);
    };

    useEffect(() => {
        callAPI();
    }, []);

    return (
        <>
            Welcome to web sample!
        </>
    );
};

export default App;